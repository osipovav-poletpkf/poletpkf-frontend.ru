$(document).ready(function () {


  var sections = $('.section');
  for (let index = 0; index < sections.length; index++) {
    //$(sections[index]).fadeOut(300).delay(1000).fadeIn(400);
  }




  releaseCarousel("CarouselHeader");
  releaseCarousel("CarouselNews");
  window.onresize = function (event) {
    onresizeCarousel("CarouselHeader");
    onresizeCarousel("CarouselNews");
  };


});



function releaseCarousel(id_carousel) {
  let carouselSlide = $("#" + id_carousel + " .carousel_slide");
  let carouselImages = $("#" + id_carousel + " .carousel_slide .carousel_item");
  let buttonPrev = $("#" + id_carousel + " .carousel_btn_prev");
  let buttonNext = $("#" + id_carousel + " .carousel_btn_next");
  let size = $("#" + id_carousel + " .carousel_container").width();
  for (let i = 0; i < carouselImages.length; i++) {
    $(carouselImages[i])
      .find(".carousel_img_bg")
      .css("width", size);
    $(carouselImages[i])
      .find(".carousel_info")
      .css("width", size);
  }

  let counter = 0;
  carouselSlide.css("transform", "translateX(" + -size * counter + "px)");
  buttonNext.click(function (e) {
    if (counter >= carouselImages.length - 1) {
      counter = 0;
      carouselSlide.css("transition", "transform 0.4s ease-in-out");
      carouselSlide.css("transform", "translateX(0px)");
      return;
    }
    counter++;
    carouselSlide.css("transition", "transform 0.4s ease-in-out");
    carouselSlide.css("transform", "translateX(" + -size * counter + "px)");
  });
  buttonPrev.click(function (e) {
    if (counter <= 0) {
      counter = carouselImages.length - 1;
      carouselSlide.css("transition", "transform 0.4s ease-in-out");
      carouselSlide.css("transform", "translateX(" + -size * counter + "px)");
      return;
    }
    counter--;
    carouselSlide.css("transition", "transform 0.4s ease-in-out");
    carouselSlide.css("transform", "translateX(" + -size * counter + "px)");
  });
  carouselSlide.on("transitionend", function () {
    if (counter >= carouselImages.length) {
      carouselSlide.css("transition", "none");
      counter = 0;
      carouselSlide.css("transform", "translateX(" + -size * counter + "px)");
    }
    if (counter < 0) {
      carouselSlide.css("transition", "none");
      counter = carouselImages.length - 1;
      carouselSlide.css("transform", "translateX(" + -size * counter + "px)");
    }
  });

}

function onresizeCarousel(id) {
  var size = $("#" + id + " .carousel_container").css("width");
  $("#" + id + " .carousel_img_bg").css("width", size);
}


