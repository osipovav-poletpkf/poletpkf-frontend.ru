$(document).ready(function () {
    $("#CarouselProductsNav .nav__menu").lavalamp({
        easing: "easeOutBack",
        duration: 1000,
        activeObj: ".current",
        setOnClick: true
    });
    $("#CarouselProductsNav .nav__menu button").click(function (e) {
        let nav_li = $(this).parent();
        $("#CarouselProductsNav .nav__menu .current").removeClass("current");
        nav_li.addClass("current");
        $("#CarouselProductsSlides .slides .current").removeClass("current");
        let slides = $("#CarouselProductsSlides .slide");
        $(slides[nav_li.index() - 1]).addClass("current");
    });



    $("#CarouselProduct .carousel_tech__nav_menu").lavalamp({
        easing: "easeOutBack",
        duration: 1000,
        activeObj: ".current",
        setOnClick: true
    });
    $("#CarouselProduct .carousel_tech__nav_menu button").click(function (e) {
        let nav_li = $(this).parent();
        $("#CarouselProduct .carousel_tech__nav_menu .active").removeClass("active");
        nav_li.addClass("active");
        $("#CarouselProduct .carousel_tech__slides .active").removeClass("active");
        let slides = $("#CarouselProduct .carousel_products__slide");
        $(slides[nav_li.index() - 1]).addClass("active");
    });

});

