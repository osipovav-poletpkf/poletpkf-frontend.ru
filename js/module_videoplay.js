$(document).ready(function () {

    var isVideoPlay = false;
    var elementVideoPlay = document.getElementById("VideoPlay");

    $('.btn_videoplay_play').click(function (e) {
        if (!isVideoPlay) {
            $('.videoplay__info').addClass('videoplay_hidden');
            $('.videoplay__cover_stop').addClass('videoplay__cover_stop_active');
            isVideoPlay = true;
            elementVideoPlay.play();
        }
    });
    $('.videoplay__cover_play').click(function (e) {
        if (!isVideoPlay) {
            $('.videoplay__info').addClass('videoplay_hidden');
            $('.videoplay__cover_stop').addClass('videoplay__cover_stop_active');
            isVideoPlay = true;
            elementVideoPlay.play();
        }
    });
    $('.videoplay__cover_stop').click(function (e) {
        if (isVideoPlay) {
            $('.videoplay__info').removeClass('videoplay_hidden');
            $('.videoplay__cover_stop').removeClass('videoplay__cover_stop_active');
            isVideoPlay = false;
            elementVideoPlay.pause();
        }
    });

});