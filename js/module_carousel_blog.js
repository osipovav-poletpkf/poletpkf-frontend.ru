$(document).ready(function () {
    $("#CarouselBlog .nav .nav__menu").lavalamp({
        easing: "easeOutBack",
        duration: 1000,
        activeObj: ".current",
        setOnClick: true
    });

    $("#CarouselBlog .nav__menu button").click(function (e) {
        console.log("button");

        let parent = $(this).parent();
        let index = parent.index();
        let slides_img = $("#CarouselBlog .slides .slide_img");
        let slides_info = $("#CarouselBlog .slides .slide_info");
        let slide_img_current = $(slides_img[index - 1]);
        let slide_info_current = $(slides_info[index - 1]);
        let slide_img_old = $("#CarouselBlog .slides_img .current");
        let slide_info_old = $("#CarouselBlog .slides_info .current");
        $("#CarouselBlog .nav .current").removeClass("current");
        $(this)
            .parent()
            .addClass("current");
        slide_img_old.removeClass("current");
        slide_info_old.removeClass("current");
        slide_img_current.addClass("current");
        slide_info_current.addClass("current");

    });

});